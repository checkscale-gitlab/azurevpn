## Getting Started
Create IAM Service Principle w/ necessary permissions and secret key
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret#creating-a-service-principal-using-the-azure-cli

Add environment variables to GitLab CI/CD
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret#configuring-the-service-principal-in-terraform

Configure on-premise vpn device and verify.
   